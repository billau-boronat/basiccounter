﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace count
{
    public class BasicCounter
    {
        private static int number;

        public BasicCounter(int number)
        {
            BasicCounter.number = number;
        }
        public static int Increment()
        {
            number += 1;
            return number;
        }
        public static int Decrement()
        {
            number -= 1;
            if (number < 0)
                number = 0;
            return number;
        }
        public static int Raz()
        {
            number = 0;
                return number;
        }
    }
}
