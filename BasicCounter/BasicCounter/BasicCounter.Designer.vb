﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.compteur = New System.Windows.Forms.Label()
        Me.Total = New System.Windows.Forms.Label()
        Me.btn_plus = New System.Windows.Forms.Button()
        Me.btn_moins = New System.Windows.Forms.Button()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'compteur
        '
        Me.compteur.AutoSize = True
        Me.compteur.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.compteur.Location = New System.Drawing.Point(272, 111)
        Me.compteur.Name = "compteur"
        Me.compteur.Size = New System.Drawing.Size(25, 25)
        Me.compteur.TabIndex = 0
        Me.compteur.Text = "0"
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total.Location = New System.Drawing.Point(262, 62)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(44, 20)
        Me.Total.TabIndex = 1
        Me.Total.Text = "Total"
        '
        'btn_plus
        '
        Me.btn_plus.Location = New System.Drawing.Point(354, 111)
        Me.btn_plus.Name = "btn_plus"
        Me.btn_plus.Size = New System.Drawing.Size(75, 23)
        Me.btn_plus.TabIndex = 2
        Me.btn_plus.Text = "+"
        Me.btn_plus.UseVisualStyleBackColor = True
        '
        'btn_moins
        '
        Me.btn_moins.Location = New System.Drawing.Point(145, 111)
        Me.btn_moins.Name = "btn_moins"
        Me.btn_moins.Size = New System.Drawing.Size(75, 23)
        Me.btn_moins.TabIndex = 3
        Me.btn_moins.Text = "-"
        Me.btn_moins.UseVisualStyleBackColor = True
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(250, 166)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(75, 23)
        Me.btn_raz.TabIndex = 4
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.btn_moins)
        Me.Controls.Add(Me.btn_plus)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.compteur)
        Me.Name = "BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents compteur As Label
    Friend WithEvents Total As Label
    Friend WithEvents btn_plus As Button
    Friend WithEvents btn_moins As Button
    Friend WithEvents btn_raz As Button
End Class
