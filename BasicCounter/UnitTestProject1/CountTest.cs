﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using count;

namespace CounterTest
{
    [TestClass]
    public class CountTest
    {
        [TestMethod]
        public void TestCount()
        {
            BasicCounter number = new BasicCounter(25);
            
            Assert.AreEqual(24, BasicCounter.Decrement());
            Assert.AreEqual(0, BasicCounter.Raz());
            Assert.AreEqual(1, BasicCounter.Increment());
            Assert.AreEqual(0, BasicCounter.Decrement());
            Assert.AreEqual(0, BasicCounter.Decrement());
        }
    }
}
